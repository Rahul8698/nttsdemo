const express = require('express');
const app = express();
const port = process.env.PORT || 8080

const middleware1 = (req, res, next) => {
    const obj = {
        fName: "Rahul",
        lName: "Singh"

    }
    req.obj = obj;
    next()
}
const middleware2 = (req, res, next) => {
    console.log("Obj from previous middleware", req.obj);
    req.obj.fName = "Lionel";
    console.log("Updated Value", req.obj)
    next()
}
// app.use('/run', middleware1)
// app.use(middleware1)
// app.use(middleware2)

app.get('/', middleware2, (req, res, next) => {
    // const error = new Error("error")
    // return next(error)
    // next(error)
    console.log(req.obj)
    res.send("Your middleware works!")
})
app.use((err, req, res, next) => {
    if (err) {
        console.log(err)
    }
    next();
})

app.listen(port, () => console.log(`Listening on port ${port}...`))
require('dotenv').config();

const express = require('express');
const app = express();
const port = process.env.PORT || 8080
const session = require('express-session');

app.use(session({
    secret: process.env.SECRET_KEY,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 1000 * 60 * 60 * 24
    }

}))

app.get('/', (req, res) => {
    if (req.session.views) {
        req.session.views++;

    } else {
        req.session.views = 1;
    }
    res.send(`No of views ${req.session.views}`)
})



app.listen(port, () => console.log(`Listening on port ${port}...`))

let arr = ['a', 'b', 'c', 'd']
for (let key of arr) {
    console.log(key)
}

function Car(name, model) {
    this.name = name,
        this.model = model

}
const c1 = new Car('Bugati', 'Vyeron')
console.log(c1.model)

const object1 = {
    a: 'somestring',
    b: 42
};

Object.entries(object1).forEach(([key, value]) => console.log(`${key} and ${value}`))
let obj = Object.values(object1)

for (let key of obj) {
    console.log(key)
}

const a = [1, 2, 3, 5, 6];
const b = {
    name: "Rahul",
    age: "27",
    hobby: () => {
        console.log("The fav game is football")
    }
}

let [e, , , f] = a;
console.log(e, f)

let { name: myName, hobby } = b;

console.log(myName)
hobby()

function x(a, b, ...rest) {
    let sum = a + b;
    for (let i of rest) {
        sum += i
    }
    return sum
}
let result1 = x(1, 2, 4, 5, 6, 8)
console.log(result1);

function y(a, b, c) {
    return a + b + c
}
console.log(y(...[1, 2, 3, 4, 5]))

const arr2 = [1, 2, 3, 4, 5];
const arr3 = ['raul', 'jdid'];

const arr4 = [...arr2, ...arr3]
arr4.push('jhf')
console.log(arr2);
console.log(arr4)

const arr6 = ['Apple', "Banana", "Orange", "Apple", "Orange", "Orange", "Mango"]

let final = arr6.reduce((result, names) => {
    if (names in result) {
        result[names]++;
    } else {
        result[names] = 1;
    }
    return result


}, {})

console.log(final)

const list = [-2, -1, 0, 5, 7, 4, 12, 9, 7, 3]

const isPrime = () => {

}

const animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];
// console.log(animals.slice(2))
// console.log(animals)

animals.splice(1, 0, 'black')
console.log(animals)
animals.splice(1, 1, 'browm')
console.log(animals)




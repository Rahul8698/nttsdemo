const express = require('express');
const app = express();
const port = process.env.PORT || 3000
const db = require('./model')
const cookieParser = require('cookie-parser')

db.sequelize.sync({
        force: false
    })
    .then(() => console.log("Connection Successful"))
app.use(cookieParser())
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}))

app.use('/api/user', require('./routes'))

app.listen(port, () => console.log(`Listening on port ${port}`))
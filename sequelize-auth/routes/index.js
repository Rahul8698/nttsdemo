const router = require('express').Router();
const {
    signUp,
    signIn,
} = require('../controller/auth');

const {
    getUser,
    updateUser,
    deleteUser,
    getAllUser

} = require('../controller/user')
const {
    auth,
    admin
} = require('../middleware/auth')


// router.get('/signin',signIn)
router.route('/signup').post(signUp)
router.route('/signin').get(signIn)

router.route('/').get(auth, getUser)
    .put(auth, updateUser)
    .delete(auth, deleteUser)

router.route('/auth').get(auth, admin, getAllUser)
module.exports = router
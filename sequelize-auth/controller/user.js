const User = require('../model').user
const jwt = require('jsonwebtoken')

exports.getUser = async (req, res) => {
    try {
        // console.log(req.user);
        const user = await User.findOne({
            where: {
                id: req.user.id
            }
        }, {
            attributes: ["name", 'email']
        })
        if (!user) {
            res.status(404).json({
                success: false,
                message: `No user exist with mail id ${email}`
            })
        }
        res.status(201).json({
            success: true,
            message: user
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.getAllUser = async (req, res) => {

    try {
        const users = await User.findAll({});
        res.status(200).json({
            success: true,
            message: users
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.updateUser = async (req, res) => {
    try {
        const user = await User.findOne({
            where: {
                id: req.user.id
            }
        })
        if (!user) {
            res.status(404).json({
                success: false,
                message: `No user exist with mail id ${email}`
            })
        }
        const dataToBeUpdated = req.body;
        const keys = [];
        for (let key in dataToBeUpdated) {
            keys.push(key)
        }
        for (let i = 0; i < keys.length; i++) {
            user[keys[i]] = dataToBeUpdated[keys[i]]
        }
        await user.save()


        res.status(200).json({
            success: true,
            message: user
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.deleteUser = async (req, res) => {
    try {
        const user = await User.destroy({
            where: {
                id: req.user.id
            }
        })
        console.log(user);
        res.status(200).json({
            success: true,
            message: "Successfully deleted:" + user.dataValues.name
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}
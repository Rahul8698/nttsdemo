const User = require('../model').user
const jwt = require('jsonwebtoken')

exports.signUp = async (req, res) => {
    try {
        const {
            name,
            email,
            password,
            isAdmin
        } = req.body;

        if (!name || !email || !password) {
            res.status(400).json({
                message: "Please Enter required fields"
            })
        }

        const user = await User.create({
            name,
            email,
            password,
            isAdmin
        })
        res.status(201).json({
            success: true,
            message: user
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.signIn = async (req, res) => {
    try {
        const {
            email,
            password
        } = req.body;

        const user = await User.findOne({
            where: {
                email
            }
        })
        if (!user) {
            res.status(404).json({
                success: false,
                message: "Email does not exist"
            })
        }
        const comparePass = await user.comparePassword(password);

        if (!comparePass) {
            res.status(403).json({
                success: false,
                message: "Invalid email or password"
            })
        }
        const token = jwt.sign({
            id: user.id,
            role: user.isAdmin
        }, "rahul", {
            expiresIn: 60 * 60 * 1000
        })
        res.status(200).cookie('token', token).json({
            success: true,
            message: user
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}
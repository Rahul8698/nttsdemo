const User = require('../model').user;
const jwt = require('jsonwebtoken');

exports.auth = (req, res, next) => {
    const {
        token
    } = req.cookies;

    if (!token) {
        res.status(401).json({
            message: "You need to login first"
        })
    }

    const decoded = jwt.verify(token, 'rahul')
    console.log(decoded);
    if (!decoded) {
        res.status(401).json({
            success: false,
            message: "You need to login first"
        })
    }
    req.user = decoded
    next()
}

exports.admin = async (req, res, next) => {

    if (req.user.role) {
        next()
    } else {
        res.status(401).json({
            success: false,
            message: "You are not authourized"
        })
    }
}
const Sequelize = require('sequelize');


const sequelize = new Sequelize('rahul', 'postgres', 'rahul2468', {
    host: "localhost",
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 1,
        idle: 1000
    }
})
const db = {};

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.user = require('./user')(sequelize, Sequelize)

module.exports = db;
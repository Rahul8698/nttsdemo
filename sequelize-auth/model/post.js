module.exports = (sequelize, DataTypes) => {
    const Post = sequelize.define('post', {
        caption: DataTypes.STRING,
        comment: {
            type: DataTypes.STRING
        },
        likes: {
            type: DataTypes.ARRAY(DataTypes.INTEGER),

        },
        image: {
            type: DataTypes.STRING,
            allowNull: false
        },
        owner: {
            type: DataTypes.STRING
        }
    }, {
        timestamps: false
    })
    return Post
}
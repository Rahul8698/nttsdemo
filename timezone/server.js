const express = require('express');
const app = express();
// const moment = require('moment-timezone');


// app.get('/time', (req, res) => {
//     // console.log(req.query);
//     const country = req.query.country;
//     const city = req.query.city
//     const date = new Date();
//     const year = date.getFullYear();
//     const month = date.getMonth();
//     const day = date.getDate();
//     const hours = date.getHours();
//     const minutes = date.getMinutes();
//     const dateFormat = year + '-' + month + '-' + day;
//     let data = moment.tz(dateFormat + ' ' + hours + ':' + minutes, `${country}/${city}`)
//     console.log(data);
//     res.json(data.format());

// })



app.get('/time', (req, res) => {
    const result = [];
    const demoObj = [
        ['en-GB', 'UTC'],
        ['en-AF', 'Asia/Kabul'],
        ['en-AD', 'Europe/Andorra'],
        ['en-AQ', 'Antarctica/Davis'],
        ['en-AQ', 'Antarctica/Troll']
    ]

    demoObj.forEach((value) => result.push(new Date().toLocaleString(value[0], { timeZone: value[1] })))

    res.json(result)
})

app.listen(3000)

// const gettimeZone = (req, res) => {
//     const result = []
//     const demoObj = [
//         ['en-GB', 'UTC'],
//         ['en-AF', 'Asia/Kabul'],
//         ['en-AD', 'Europe/Andorra'],
//         ['en-AQ', 'Antarctica/Davis'],
//         ['en-AQ', 'Antarctica/Troll']
//     ]

//     demoObj.map(value => {
//         result.push(new Date().toLocaleString(value[0], { timeZone: value[1] }))
//     })
//     res.send(result)
// }




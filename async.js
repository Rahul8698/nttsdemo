const register = () => {
    return new Promise((res, rej) => {
        if (true) {
            setTimeout(() => {
                res("User registered")
            }, 2000)
        }
    })
}

const login = () => {
    return new Promise((res, rej) => {
        if (true) {
            setTimeout(() => {
                res("User login")
            }, 1000)
        }
    })
}

const result = async () => {
    await register().then(data => console.log(data));
    await login().then(data => console.log(data));
}

result()
// const { rejects } = require("assert")
// const { resolve } = require("path")

// const prom = (x) => {
//     return new Promise((res, rej) => {
//         if (typeof x === 'number') {
//             res("The condition matched")
//         }
//         rej("The value passed is not a number")

//     })
// }

// prom(12)
//     .then((data) => console.log(data))
//     .catch((err) => console.log(err))



// const countryOfResidence = (name, country) => {
//     return new Promise((res, rej) => {
//         setTimeout(() => {
//             console.log(`${name} lives in ${country}!`)
//             res();
//         }, 1000)
//     })
// }

// const profession = (prof) => {
//     return new Promise((res, rej) => {
//         setTimeout(() => {
//             console.log(`He is a ${prof}`)
//             res();
//         }, 0000)
//     })
// }

// // countryOfResidence('John Wick', 'NoWhere')
// //     .then(() => profession('Assassin'))
// //     .catch((err) => console.log(err))



// const sum = (a) => {
//     return new Promise((resolve, reject) => {
//         if (a) {
//             setTimeout(() => {
//                 let x = Math.floor(Math.random() * 10);
//                 resolve(x)
//             }, 1000)
//         } else {
//             reject("Something went wrong!")
//         }
//     })
// }
// sum(2)
//     .then((resp) => {
//         console.log("The random number square is:", resp * resp)
//         return sum(resp)
//     })
//     .then((data) => console.log(data * data))
//     .catch((err) => console.log(err))



const prom1 = (n) => {
    return new Promise((res, rej) => {
        if (n) {
            setTimeout(() => {
                res(n)
            }, 1000)
        } else {
            rej('Error')
        }
    })
}

prom1('')
    .then((data) => {
        console.log('The num is,', data)
        return prom1(data)
    })
    .then((data) => {
        console.log('The number is', data)
        return prom1(data)
    })
    .catch((err) => {
        console.log(err)
        return prom1('')
    })
    .then((data) => {
        console.log("the number is", data)
        return prom1(data)
    })

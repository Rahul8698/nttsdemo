const express = require('express');
const app = express();
const port = 5000;
const route = require('./route')
const path = require('path')

require('./config/db')()

global.root = path.resolve(__dirname)


app.use(express.json());
app.use(express.urlencoded({ extended: true })) //querystring library parse-false qs library parse-true




app.use('/api/v1', route)
// package.json and package-lock

app.listen(port, () => console.log(`Listening on port ${port}`))
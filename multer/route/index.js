/*The express.Router() function is used to create a new router object. 
This function is used when you want to create a new router object in your program to handle requests.*/

const router = require('express').Router()
const upload = require('../utils').array('image', 10)

const file = require('../controller')

// const { fileUploader } = require('../controller')

router.post('/upload', upload, file.fileUploader)

module.exports = router

//learn module.exports and exports bookmarked


// This wrapper function has 5 arguments: exports, require, module, __filename, and __dirname
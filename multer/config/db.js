const mongoose = require('mongoose');


const db = () => {
    mongoose.connect('mongodb://localhost:27017/upload');
    const connection = mongoose.connection

    connection.once('open', () => console.log('Database connected'))
    connection.on('error', () => console.log('Something went wrong'))
}

module.exports = db
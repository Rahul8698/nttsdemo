
const File = require('../model')
const fs = require('fs')

exports.fileUploader = async (req, res) => {
    const { name } = req.body

    let images;
    try {
        images = req.files.map(img => {
            return img.filename
        })

        const files = await File.create({
            name,
            images
        })

        res.status(200).json(files)
    } catch (err) {
        // console.log(err)
        for (let file of req.files) {
            console.log(file);
            fs.unlink(`${root}/${file.path}`, (err) => {
                if (err) {
                    console.log(err)
                }
            })
        }
    }
}
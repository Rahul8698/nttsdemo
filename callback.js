//Example-1

// function result(a, b, cb) {

//     console.log(`The two numbers to add are ${a} and ${b}`)
//     cb(a, b)
// }
// function add(a, b) {
//     console.log(a + b)
// }

// result(4, 5, add);

//Example-2

function a(cb) {
    setTimeout(() => {
        console.log("Hi i am a setTimeout Function")
        cb()
    }, 2000)
}

function b(cb1) {
    setTimeout(() => {
        console.log('I am going to take some time to execute');
        cb1()
    }, 3000)
}

function c() {
    setTimeout(() => {
        console.log("I am third in the line of execution");
    }, 1000)
}
a(function () {
    b(function () {
        c()
    })
})

//Example-3

function addOn(cb) {

    setTimeout(() => {
        let x = Math.floor(Math.random() * 10)

        cb(x)
        console.log('The number is:', x)
    })
}

function callback(n) {
    for (let i = 1; i <= 10; i++) {
        setTimeout(() => {
            console.log(i + n);
        }, i * 1000)

    }
}

addOn(callback)

const file = require('fs')

file.writeFile('', 'Hello world', 'utf-8', (err) => {
    if (err) {
        return console.log(err)
    }
    console.log('File created')

})

//Named Function 
function add(a, b) {
    let sum = a + b;
    return function (c) {
        return sum * c;
    }
}

// let result = add(4, 5)
// console.log(result(9))

//Immediately Invoke Function

(function () {
    for (let i = 1; i <= 10; i++) {
        console.log(i)
    }
})

//Arrow Function

const arrow = (num) => {
    return num * num
}
console.log(arrow(5))

function greet(msg) {

    return function (name) {
        console.log(msg + " " + name)
    }
}

let res = greet('Hi')
res('Rahul')
const express = require('express')
const cluster = require('cluster');
const os = require('os')
const app = express()
const cpus = os.cpus().length;



app.get('/', (req, res) => {
    res.send(`ok ${process.pid}`)
    cluster.worker.kill()
})
console.log(cluster.isMaster, cpus);
if (cluster.isMaster) {
    for (let i = 0; i < cpus; i++) {
        cluster.fork();
    }
    cluster.on('exit', (worker) => {
        console.log(`worker ${worker.process.pid} died`);
        cluster.fork()
    })
} else {
    app.listen(3000, () => console.log(`server ${process.pid} @http://localhost:3000 `))
}
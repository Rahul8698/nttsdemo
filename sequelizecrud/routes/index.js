const router = require('express').Router();
const {
    createTutorial,
    createComment,
    findTutorialById,
    findCommentById
} = require('../controller/tutorial');

router.route('/tutorial').post(createTutorial)

router.route('/tutorial/:id').get(findTutorialById)

router.route('/comment').post(createComment)

router.route('/comment/:id').get(findCommentById)
module.exports = router
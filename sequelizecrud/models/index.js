const Sequelize = require('sequelize');
const dbConfig = require('../config/db')

const sequelize = new Sequelize(dbConfig.db, dbConfig.user, dbConfig.password, {
    host: dbConfig.host,
    dialect: dbConfig.dialect,
    pool: dbConfig.pool
})

const db = {};

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.tutorials = require('./tutorial')(sequelize, Sequelize);
db.comments = require('./comment')(sequelize, Sequelize);
db.tutorials.hasMany(db.comments, {
    foreignKey: "tutorialId",
    as: "comments"
})
db.comments.belongsTo(db.tutorials, {
    foreignKey: "tutorialId",
    as: "tutorial"
})

module.exports = db;
const express = require('express');
const app = express();
const db = require('./models')

db.sequelize.sync({
        force: false
    })
    .then(() => console.log("re-sync table"))

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}))

app.use('/api', require('./routes'))
app.use((req, res) => {
    res.status(404).json({
        message: "Url not found"
    })
})
app.listen(5000, () => console.log("Listening on port 5000"))
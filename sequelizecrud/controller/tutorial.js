const db = require('../models');
const Tutorial = db.tutorials;
const Comment = db.comments;

exports.createTutorial = async (req, res) => {
    try {
        const {
            title,
            description
        } = req.body
        const tutorial = await Tutorial.create({
            title,
            description
        })
        res.status(201).json({
            success: true,
            message: tutorial
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.createComment = async (req, res) => {
    try {
        const {
            name,
            text,
            tutorialId
        } = req.body;
        const comment = await Comment.create({
            name,
            text,
            tutorialId
        })
        res.status(201).json({
            success: true,
            message: comment
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.findTutorialById = async (req, res) => {
    try {
        const tutorial = await Tutorial.findOne({
            include: {
                model: Comment,
                as: "comments"

            }
        }, {
            where: {
                id: req.params.id
            }
        })
        res.status(200).json({
            success: true,
            message: tutorial

        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.findCommentById = async (req, res) => {
    try {
        const comment = await Comment.findOne({
            where: {
                id: req.params.id
            },
            include: {
                model: Tutorial,
                as: "tutorial"
            }
        })
        res.status(200).json({
            success: true,
            message: comment
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}